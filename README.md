# R Package `s170R`

## Author

Isaac Quintanilla Salinas

## General Information

This repository contains information on `s170R` R Package. It contains [learnr](https://rstudio.github.io/learnr/) tutorials for my students to complete. The package will install the `learnr` and the [`tidyverse`](https://www.tidyverse.org/) package as well. RStudio is required to run the tutorials. You can download the latest version of RStudio [here](https://rstudio.com/products/rstudio/download/#download). Additionally, the package will install [`tinytex`](https://yihui.org/tinytex/) to install a local LaTeX distribution on your computer.

If there are any questions or there are any errors in the tutorial, please feel free to email me.


## Install R Package

To install the package, use the following code:

    #install.packages("devtools")
    #Intsall R Package
    devtools::install_gitlab("inqs909/s170R")

Make sure to install the `devtools` package first.


### Windows

You will need to install rtools to allow the `devtools` package to install packages from a remote repository: [https://cran.r-project.org/bin/windows/Rtools/](https://cran.r-project.org/bin/windows/Rtools/).

Make sure to update R as well.

## RMD Templates

There is an RMD template available for you to use for your homework assignment. When you create a new RMD File, you can choose "HW" from the template section to create it. The template contains basic formatting and instruction to install a LaTeX distribution with [`tinytex`](https://yihui.org/tinytex/). However, I do recommend installing LaTeX [website](https://www.latex-project.org/) and install the distribution you need: [Windows](https://miktex.org/), [Mac](http://www.tug.org/mactex/), or Linux ([Debian](https://linuxconfig.org/how-to-install-latex-on-ubuntu-18-04-bionic-beaver-linux) or [RHEL](https://docs.fedoraproject.org/en-US/neurofedora/latex/)).

## learnr Tutorials

This package contains a `learnr` tutorials that can be used to learn about different statistical topics. View the vignette on tutorials to get more information. Go to the articles section of the website to find the page.

### Access learnr tutorial

To access one of my learnr tutorials, use the `access_tutorial()` function. It only needs the name (id) of the tutorial.

    library(s170R)
    access_tutorial("R_Basics")

## R Package Misc

This package is designed to provide my Upward Bound students with a tutorial on different topics and get an idea about programming in R. Therefore, there are 3 built-in functions designed to help students access the tutorial, record their answers and have obtain a csv file to submit for grading. The functions are the `access_tutorial()`, `getting_work()`, and `writing_tutorial()` functions.

### getting\_work()

My learnr tutorials record a student's activity and saves it in a CSV file. Students can turn in this CSV file as there submission. To retrieve the file, use the `getting_work()` function. It will obtain the file and save your file in the working directory and named as "YOUR\_NAME-INTRO\_R-Submission.csv". This function needs the name of the tutorial and your name to work.

    getting_work("Intro_R","YOUR_NAME")

### writing\_tutorial()

The `writing_tutorial()` function is used to record a students progress. As the student progresses from the learnr tutorial, it will record an action is committed. The action is then saved in a CSV file that a student can later obtain using the `getting_work()` function. The `writiing_tutorial()` is placed at the beginning of an Rmd file to record a students' work.
